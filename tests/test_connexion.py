from datetime import datetime
from pandas.io.sql import read_sql
import datetime as dt
import pandas as pd
import numpy as np
import pymysql
import urllib
import platform



class sql_connector:
  
    _PROD_SERVER = 'KECH-DB03:1447'   
    _MATLAB_SERVER = '172.29.53.178:3306'
    _FLEXPORT_SERVER = 'KECH-DB04:1447'
    _MATLAB_LOGIN = {'admin':'Paris4ever!!'}
    _REPOSITORY_DB = 'KGR'
    _MARKET_DATA_DB = 'MARKET_DATA'
    _QUANT_DB = 'QUANT'
    _FLEXPORT_DB = 'FLEX_EXPORT'
    _TIMEOUT = 300 # TIMEOUT en secondes.
  
    def __init__(self):
        from sqlalchemy import create_engine
        self.engine_creator = create_engine
    
    def connection(self,server='production'):
        if server=='production':
            if platform.system() == 'Windows' : 
                connection_str = "mssql+pymssql://@%s/%s" % (self._PROD_SERVER,self._QUANT_DB) 
                engine = self.engine_creator(connection_str, legacy_schema_aliasing=False, connect_args={'timeout': self._TIMEOUT})
            else : 
                connection_str = 'DRIVER={ODBC Driver 17 for SQL Server};INSTANCE=BDDINST01;SERVER=KECH-DB03.keplercm.lan;PORT=1433;DATABASE=QUANT;UID=quant-bitbucket;PWD=Welcome2022!'
                params = urllib.parse.quote_plus(connection_str)
                engine = self.engine_creator("mssql+pyodbc:///?odbc_connect=%s" % params)
        elif server=='Matlab':
            connection_str = "mysql+pymysql://%s:%s@%s" % ('admin','Paris4ever!!',self._MATLAB_SERVER)
            engine = self.engine_creator(connection_str)
        
        elif server=="FLEXPORT":
            connection_str = "mssql+pymssql://@%s/%s" % (self._FLEXPORT_SERVER,self._FLEXPORT_DB)
            engine = self.engine_creator(connection_str, legacy_schema_aliasing=False, connect_args={'timeout': self._TIMEOUT})
        return engine


class my_env:
    read_only_repository = "x:\\kc_repository_new"
    get_tick = "get_tick\\ft"

def test_connection() : 
    con = sql_connector().connection()
    query = """select date, security_id, trading_destination_id from MARKET_DATA..trading_daily where security_id = 110"""
    df_trading_data = pd.read_sql(query,con)
    return df_trading_data